import Vue from 'vue';
import App from './App.vue';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import axios from 'axios';
import VueAxios from 'vue-axios';
import router from './routes';
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

const Axios = axios.create({
  baseURL: process.env.VUE_APP_API,
  withCredentials: false
});

Vue.config.productionTip = false;

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.use(Buefy);
Vue.use(VueAxios, Axios);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
