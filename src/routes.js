import Vue from 'vue';
import VueRouter from 'vue-router';
import Cities from './components/Cities.vue';
import Sellers from './components/Sellers.vue';


Vue.use(VueRouter);

const routes = [
    { path: '/cities', component: Cities },
    { path: '/sellers', component: Sellers }
];

const router = new VueRouter ({
    base: process.env.BASE_URL,
    routes
});

export default router;
